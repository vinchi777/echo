crumb :root do
  link "Home", root_path
end

crumb :requests do
  link "Requests", requests_path
end

crumb :my_requests do
  link "My Requests", owner_requests_path
end

crumb :index_requests do
  link "All Requests", requests_path
end

crumb :request_proposals do |request|
  link "#{request.service.type.capitalize} poposals", proposals_customers_path(request_id: request.id)
  parent :my_requests
end

crumb :new_job_offer do |job_offer|
  link "Job offer", new_job_offer_path(proposal_id: job_offer.proposal.id)
  parent :request_proposals, job_offer.request
end

crumb :show_job_offer do |job_offer|
  link "Job offer", job_offer_path
  parent :request_proposals, job_offer.request
end

crumb :my_proposals do
  link "My Proposals", owner_proposals_path
end

crumb :index_proposals do
  link "Proposals for me", proposals_path
end

crumb :new_proposal do |proposal|
  link "New Proposal", new_proposal_path(request_id: proposal.request_id)
  parent :index_requests
end

crumb :show_proposal do |proposal|
  link "Proposal for #{proposal.request.service.type}", proposal_path(proposal)
  if proposal.business.user != current_user
    parent :index_proposals
  else
    parent :my_proposals
  end
end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).
