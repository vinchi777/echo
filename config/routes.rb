Rails.application.routes.draw do
  resources :conversations
  resources :reviews
  resources :request_images
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount ActionCable.server => '/cable'

  devise_for :users
  resources :users do
    get 'profile', to: 'users#show'
    get 'edit_profile', to: 'users#edit'
  end

  resources :sellers do
    collection do
      get 'dashboard', to: 'sellers#dashboard'
      get 'proposals', to: 'sellers#proposals'
    end
  end

  resources :customers do
    collection do
      get 'dashboard', to: 'customers#dashboard'
      get 'proposals', to: 'customers#proposals'
    end
  end

  resources :services do
    collection do
      get 'categories', to: 'services#categories'
      get '/area/:area', to: 'services#area', as: :area
      get '/area/:area/:category', to: 'services#category', as: :category
      get 'search', to: 'services#search'
    end
  end

  resources :requests do
    collection do
      get 'owner', to: 'requests#owner'
    end
    member do 
      get 'proposals', to: 'requests#proposals'
    end
  end
  resources :request_images

  resources :businesses
  resources :messages do
    collection do
      get 'conversations', to: 'messages#conversations'
      get 'conversations/:id', to: 'messages#conversations'
    end
  end

  resources :job_offers do
    member do
      post 'accept', to: 'job_offers#accept'
    end
  end

  resources :proposals do
    collection do
      get 'owner', to: 'proposals#owner'
    end
    member do
      post 'book', to: 'proposals#book'
    end
  end

  resources :reviews
  resources :notifications

  get '/how-it-works', to: 'pages#how_it_works'
  root :to => 'pages#home'
end
