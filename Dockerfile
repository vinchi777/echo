FROM ruby:2.3.3

ARG rails_env=development

# Install rails dependencies
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev curl

# Install nodejs
RUN curl -sL https://deb.nodesource.com/setup_7.x | /bin/bash
RUN apt-get install nodejs

RUN apt-get install -y imagemagick

# Install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn

RUN mkdir /echo
WORKDIR /echo

# Install gems
COPY Gemfile /echo/Gemfile
COPY Gemfile.lock /echo/Gemfile.lock
RUN bundle install

# Install client libraries
COPY package.json yarn.lock ./
RUN yarn install --pure-lockfile

# Copy the main application
COPY . /echo

# Precompile Rails assets 
RUN bundle exec rake assets:precompile RAILS_ENV=$rails_env

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]

