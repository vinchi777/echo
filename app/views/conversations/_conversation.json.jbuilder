proposal = conversation.proposal
requester = conversation.request.user
seller = proposal.business.user

json.extract! conversation, :_id, :created_at, :updated_at, :proposal_id, :request_id
json.url conversation_url(conversation, format: :json)

json.request proposal.request
json.service_type proposal.request.service.type

json.requester do
  json.partial! 'users/user', user: requester
end

json.seller do
  json.partial! 'users/user', user: proposal.business.user
end

json.receiver do
  receiver = seller != current_user ? seller : requester
  json.partial! 'users/user', user: receiver
  if receiver == seller
    json.business do
      json.partial! 'businesses/business', business: proposal.business
    end
  end
end



