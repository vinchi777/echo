json.data do
  json.merge! @message.as_json
  json.user do
    json.partial! 'users/user', user: @message.user
  end
end
