json.data @proposal_messages do |proposal|
  #json.(proposal, :price_quotation, :payment_mode, :_id)
  json.merge! proposal.as_json

  json.request proposal.request
  json.service_type proposal.request.service.type

  json.requester do
    json.partial! 'users/user', user:  requester
  end 

  json.seller do
    json.partial! 'users/user', user: seller
  end

  json.receiver do
    receiver = seller != current_user ? seller : requester
    json.partial! 'users/user', user: receiver
    if receiver == seller
      json.business do
        json.partial! 'businesses/business', business: proposal.business 
      end
    end
  end


end
