json.extract! request_image, :id, :created_at, :updated_at
json.url request_image_url(request_image, format: :json)
