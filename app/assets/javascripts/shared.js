$( document ).on('turbolinks:load', function() {
  $(".button-collapse").sideNav()
  $('select').material_select();

  $('.dropdown-button').dropdown()

  Materialize.updateTextFields();

  if ($('.flash-dialog').length){
    Materialize.toast($('.flash-dialog'), 4000)
  }

  if ( $('.slick').length ) {
    $('.slick').slick({
      slidesToShow: 4,
      arrows: true,
      infinite: false,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
    });
  }

  if ($('#js-locations-modal').length) {
    $('#js-locations-modal').modal()
  }

  $('.js-location-input-container').on('click', function(){
    $('#js-locations-modal').modal('open')
  })

  $('.datepicker').pickadate()


  $(function(){
    $.ajax({
      type: 'GET',
      url: '/services/categories.json',
      success: function(response){
        $('.js-autocomplete').autocomplete({
          data: response['data'],
          limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
          onAutocomplete: function(val) {
            id = val.replace(" ", "-")

            $('.service-type-checkboxes').hide()
            $('.service-type-checkboxes input').attr('disabled', true)

            $('#'+ id).show()
            $('#'+ id +' input').attr('disabled', false)

            // Callback function when value is autcompleted.
          },
          minLength: 0, // The minimum length of the input for the autocomplete to start. Default: 1.
        });

      }

    })

  })
  window.scrollTo(0, $('.message-list-container').scrollHeight);

})

$(document).on('turbolinks:before-cache', function(){
  if($('.slick').length){
    $('.slick').slick('unslick')
  }
})



//FUNCTIONS
function showProvinceCollection(key){
  $('#js-city-collection').hide()
  $('#js-'+ key +'-collection').show()
}

function setLocation(province, city){
  $('.js-location-input').val(province + ', ' + city.replace("-", " "))
  $('#js-locations-modal').modal('close')
  Materialize.updateTextFields();
}
function backToCity(city) {
  $('.js-province-collection').hide()
  $('#js-city-collection').show()
}
