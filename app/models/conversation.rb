class Conversation
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :messages
  has_and_belongs_to_many :users
  belongs_to :proposal
  belongs_to :request

  field :messages_count, type: Fixnum, default: 0
  field :messaged_at, type: DateTime
end
