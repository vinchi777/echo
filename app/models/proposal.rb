class Proposal
  include Mongoid::Document
  include Mongoid::Timestamps
  include PublicActivity::Common

  belongs_to :request
  belongs_to :business
  has_many :messages
  has_one :job_offer
  has_one :conversation

  field :price_quotation,               type: Integer
  field :payment_mode,                  type: String
  field :job_delivery_estimate,         type: String
  field :job_delivery_estimate_unit,    type: String
  field :message,                       type: String
  field :booked,                        type: Boolean, default: false
  field :status,                        type: String, default: 'active' #active, pending, expired, done
  field :accepted,                      type: Boolean, default: false

  validates :message, presence: true
  validates :status, presence: true, inclusion: { in: ['active', 'pending', 'expired', 'done'] }
  validates :payment_mode, presence: true, inclusion: { in: ['single', 'hourly', 'daily', 'monthly'] }
  validates :job_delivery_estimate_unit, presence: true, inclusion: { in: ['hours', 'days', 'months'] }

  after_create :email_proposal_sent,
    :create_conversation,
    :create_notification

  def create_conversation
    return if self.conversation
    self.build_conversation(request: request).save!
    self.conversation.users << business.user
    self.conversation.users << request.user
  end

  def create_notification
    self.create_activity(:create, owner: business.user, recipient: request.user, recipient_read: false)
  end

  def email_proposal_sent
    ProposalMailer.proposal_sent(self).deliver
    RequestMailer.proposal_received(self).deliver
  end

end
