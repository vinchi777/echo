class Service
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :requests
  has_many :questions

  field :area,                  type: String
  field :category,              type: String
  field :type,                  type: String
  field :description,           type: String
  field :has_upload,            type: Boolean, default: false
  field :available,             type: Boolean, default: true

  mount_uploader :image, ServiceImageUploader

  validates :area, presence: true, inclusion: { in: ['home', 'self', 'events'] }
  validates :category, presence: true
  validates :type, presence: true, uniqueness: true

  before_save :downcase_names


  private

  def downcase_names
    self.category = self.category.downcase
    self.type = self.type.downcase
  end
end
