class Request
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user
  belongs_to :service
  has_many :proposals
  has_many :answers
  has_many :messages
  has_many :request_images
  has_one :job_offer

  field :title,                 type: String
  field :description,           type: String
  field :location,              type: String
  field :max_price,             type: Integer, default: 0
  field :min_price,             type: Integer, default: 0
  field :payment_mode,          type: String
  field :job_completion,        type: String
  field :job_completion_unit,   type: String
  field :specifics,             type: String
  field :start_date,            type: Date
  field :done,                  type: Boolean, default: false
  field :status,                type: String, default: 'active' # active, pending, expired, done
  field :has_accepted_proposal,     type: Boolean, default: false 

  validates :location, presence: true
  validates :status, presence: true, inclusion: { in: ['active', 'pending', 'expired', 'done'] }
  validates :payment_mode, presence: true, inclusion: { in: ['single', 'hourly', 'daily', 'monthly'] }
  validate :validate_request_images

  after_create :email_request_completion

  def accepted_proposal
    proposals.where(:accepted => true).first
  end

  def validate_request_images
    errors.add(:request_images, "too much") if request_images.size > 3
  end

  def email_request_completion
    RequestMailer.request_completion(self).deliver
  end

end
