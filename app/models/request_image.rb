class RequestImage
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :request
  mount_uploader :image, RequestImageUploader
end
