class Review
  include Mongoid::Document
  include Mongoid::Timestamps
  include PublicActivity::Common

  belongs_to :business
  belongs_to :user, optional: true

  field :good,          type: Boolean, default: false
  field :bad,           type: Boolean, default: false
  field :message,       type: String
  field :name,          type: String
  field :email,         type: String
  field :show,          type: Boolean, default: true
end
