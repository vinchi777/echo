class Message
  include Mongoid::Document
  include Mongoid::Timestamps

  field :body,        type: String
  field :read,        type: Boolean, default: false
  field :user_role,   type: String

  belongs_to :user
  belongs_to :conversation, counter_cache: true

  validates :body, presence: true
  validates :user_role, presence: true, inclusion: {in: ['customer', 'seller']}
end
