class Business
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user
  has_and_belongs_to_many :services, inverse_of: nil
  has_many :proposals
  has_many :reviews

  field :name,                  type: String
  field :status,                type: String
  field :nature_of_business,    type: String

  mount_uploader :image, BusinessImageUploader

  validates :user_id, presence: true
  validates :name, presence: true

  after_create :notify_business_setup

  def proposal(request)
    Proposal.where(:request_id => request.id, :business_id => self.id).first
  end

  def image_url
    if image.file
      image.url
    else
      user.avatar_url
    end
  end

  def notify_business_setup
    BusinessMailer.setup_notification(self).deliver
  end
end
