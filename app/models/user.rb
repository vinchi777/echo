class User
  include Mongoid::Document
  include Mongoid::Timestamps

  has_one :business
  has_many :requests
  has_many :messages

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  field :is_customer,       type: Boolean, default: false
  field :is_seller,         type: Boolean, default: false
  field :current_role,      type: String,  default: 'customer'
  field :phone,             type: String
  field :first_name,        type: String
  field :last_name,         type: String

  mount_uploader :avatar, AvatarUploader

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :current_role, inclusion: {in: ['customer', 'seller']}, allow_blank: true

  after_create :notify_welcome_email

  def customer_role?
    current_role == 'customer'
  end

  def seller_role?
    current_role == 'seller'
  end

  def complete_name
    "#{first_name} #{last_name}"
  end

  # priorit of avatar
  # 1 image upload
  # 2 google/facebook
  # 3 gravatar
  # 4 first letter
  def avatar_url(opts={})
    if avatar.file
      avatar.url
    else
      "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(self.email)}?s=#{opts.delete(:size) { 40 }}"
    end
  end

  def notify_welcome_email
    UserMailer.welcome_notification(self).deliver
  end

end
