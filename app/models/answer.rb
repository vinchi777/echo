class Answer
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :request
  has_one :question

  field :order,         type: Integer
  field :field_type,    type: String
  field :answered,      type: Boolean, default: false
  field :result,        type: String
end
