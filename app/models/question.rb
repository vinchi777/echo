class Question
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :service

  field :sentence,              type: String
  field :field_type,            type: String # Select, Radio, Checkbox, Input, Upload
  field :order,                 type: Integer, default: 0
  field :optional,              type: Boolean, default: false
  field :multple_answers,       type: Boolean, default: false
  field :choices,               type: Array, default: []
  field :other,                 type: String
  field :has_other,             type: Boolean, default: false
end
