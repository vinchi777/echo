class JobOffer
  include Mongoid::Document
  include Mongoid::Timestamps
  include PublicActivity::Common

  belongs_to :proposal
  belongs_to :request

  field :agreed_job_completion,         type: Integer
  field :agreed_job_completion_unit,    type: String
  field :agreed_price,                  type: Integer
  field :agreed_payment_mode,           type: String
  field :order_id,                      type: String
  field :message,                       type: String
  field :status,                        type: String, default: 'pending'
  field :is_accepted,                   type: Boolean, default: false
  field :accepted_at,                   type: DateTime

  validates :status, presence: true, inclusion: { in: ['pending', 'active', 'expired', 'done'] }
  validates :agreed_job_completion_unit, inclusion: { in: ['hours', 'days', 'months'] }, allow_blank: true
  validates :agreed_payment_mode, inclusion: { in: ['single', 'hourly', 'daily', 'monthly'] }, allow_blank: true

  def accept!
    self.is_accepted = true
    self.accepted_at = DateTime.now
    self.status = 'active'
    self.save!
  end

  def worker
    proposal.business.user
  end
end
