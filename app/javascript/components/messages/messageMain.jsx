import React, { PropTypes, Component } from 'react'
import ReactDOM from 'react-dom'
import MessageItem from './messageItem'
import MessageForm from './messageForm'

class MessageMain extends Component {
  constructor(props){
    super(props)
    this.state = {
      messages: [],
      conversation: {}
    }
    this.fetchMessages(props.match.params.convId, this.props.conversation)
  }
  componentDidMount(){
    this.scrollToBottom()
  }
  componentDidUpdate(){
    this.scrollToBottom()
  }
  componentWillReceiveProps(nextProps){
    this.fetchMessages(nextProps.match.params.convId, nextProps.conversation)
  }
  scrollToBottom(){
    var node = document.getElementById("js-scroll-bottom")
    node.scrollTop = node.scrollHeight;
  }
  fetchMessages(convId, conversation){
    var url = "/messages.json?" + $.param({conversation_id: convId})

    fetch(url, {
      credentials: "same-origin"
    }).then( res => res.json() )
      .then( messages => {
        if(this.refs.messageMain) {
          this.setState({ messages: messages.data, conversation: conversation})
          this.setupSubscription()
        }
      })

  }
  setupSubscription(){
    App.comments = App.cable.subscriptions.create(
      {channel: "ConversationsChannel", conversation_id: this.state.conversation._id},
      {
        connected: function () {
          console.log('connected')
        },

        received: (data) => {
          const joined = this.state.messages.concat(data.data)
          this.setState({messages: joined})
        },

        updateCommentList: this.updateCommentList

      }
    )
  }
  onSubmit(msg){
    const joined = this.state.messages.concat(msg)
    this.setState({messages: joined})
  }
  render(){
    return(
      <div className='message-main card no-margin' ref="messageMain">
        <div className='conversation-title blue card-content '>
          <span className='white-text'> {  this.state.conversation.service_type } </span>
          <a href={'/proposals/' + this.state.conversation.proposal_id} className='white-text right'>View Proposal</a>
        </div>
        <div id='js-scroll-bottom' className='message-list card-content' ref={ (el) => { this.messagesEnd = el } }>
          { this.state.messages.map( (msg,i) =>
            <MessageItem  key={i} message={msg}/>
          ) }
        </div>
        <div className='message-footer card-content no-padding-top'>
          <MessageForm  onSubmit={ this.onSubmit.bind(this) } conversation={ this.state.conversation } />
        </div>
      </div>
    )
  }
}

export default MessageMain
