import React, { Component } from 'react'
import moment from 'moment'

class MessageItem extends Component {
  constructor(props){
    super(props)
    this.state = { msg: props.message }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({msg: nextProps.message})
  }
  render(){
    const msg = this.state.msg
    return (
      <div>
        <img src={msg.user.avatar_url} className='responsive-img circle' width='30px' />
        <small>{ moment(msg.created_at).fromNow() }</small>
        <p>
          { msg.body }
        </p>
      </div>
    )
  }
}

export default MessageItem
