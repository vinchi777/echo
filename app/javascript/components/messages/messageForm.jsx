import React, { PropTypes, Component } from 'react'

class MessageForm extends Component {
  constructor(props){
    super(props)
  }
  handleSubmit(event){
    event.preventDefault()
    if (!this.props.conversation) { return false }

    const message = {
      body: this.body.value,
      conversation_id: this.props.conversation._id
    }
    var token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
      type: 'POST',
      url: '/messages.json',
      data: {
        message: message
      },
      complete: () => {
        console.log('clear')
        this.clearForm()
      }
    })
  }
  clearForm(){
    this.body.value = null
  }
  render(){
    return(
      <form onSubmit={this.handleSubmit.bind(this)}>
        <div className='input-field'>
          <textarea className='materialize-textarea' name='body' ref={(input) => this.body = input} placeholder='Enter message ...' rows="1"></textarea>
        </div>

        <input type='submit' className='btn' />
      </form>
    )
  }
}

export default MessageForm
