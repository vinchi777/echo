import React, { PropTypes, Component } from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import MessageMain from './messageMain'



class MessageList extends Component {
  constructor(props) {
    super(props)
    const _this = this
    this.state = {
      conversations: []
    }
    fetch('/conversations.json', {
      credentials: "same-origin"
    }).then( res => res.json() )
      .then( conversations => {
        this.setState({ conversations: conversations.data })
      })
  }
  navUser(receiver) {
    if(receiver.business) {
        return(<p> { receiver.business.name }</p>)
    } else {
        return(<p> { receiver.first_name }</p>)
    }
  }
  findConv(convId) {
    if (this.state.conversations.length) {
      return this.state.conversations.find(conv => conv._id === convId)
    } else{
      return {}
    }
  }
  render() {
    const { conversations } = this.state

    return(
      <Router>
        <div className="tabs-vertical row">
          <div className='col s12 m4 l4'>
            <ul className='tabs card' ref='tabs'>
              { conversations.map( (conv, i) =>
                <li className='tab tab-thumbnail' key={i}>
                  <NavLink to={`/messages/conversations/${conv._id}`} data-turbolinks='false' activeClassName='active' className='message-tab'>
                    <div className='col s4 m4'>
                      <img src={conv.receiver.business ? conv.receiver.business.image_url : conv.receiver.avatar_url} className='responsive-img circle' width='40px' />
                    </div>

                    <div className='col s8 m8 no-padding-left'>
                      { this.navUser(conv.receiver) }
                    </div>
                  </NavLink>
                </li>
              ) }
            </ul>
          </div>

          <div className='col s12 m8 l8'>
            <Route exact path="/messages/conversations" render={ () => (
              conversations.length ? (<Redirect to={`/messages/conversations/${conversations[0]._id}`}/>) : (<p></p>)
            ) } />

            <Route path={`/messages/conversations/:convId`} component={ ({ match }) => (
              <MessageMain match={match} conversation={this.findConv(match.params.convId)}/>
            ) } />
          </div>
        </div>
      </Router>
    )
  }

}

export default MessageList
