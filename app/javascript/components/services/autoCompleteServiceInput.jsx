import React, { PropTypes, Component } from 'react'
import ReactDOM from 'react-dom'

class AutoCompleteServiceInput extends Component {
  constructor(props) {
    super(props)
    this.state = { query: '' }
  }
  handleChange(e) {
    this.setState({query: e.target.value})
  }
  componentDidMount(){
    var _this = this

    $.ajax({
      type: 'GET',
      url: '/services/categories.json',
      success: function(response){
        $(ReactDOM.findDOMNode(_this.refs.autocomplete)).autocomplete({
          data: response['data'],
          limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
          onAutocomplete: function(val) {
            // Callback function when value is autcompleted.
          },
          minLength: 0, // The minimum length of the input for the autocomplete to start. Default: 1.
        });

      }

    })
  }
  render() {

    return (
      <div>
        <i className="material-icons prefix">build</i>
        <input type="text" name="service" className="autocomplete" ref="autocomplete" autoComplete="off" id="autocomplete-input" defaultValue={this.props.value}/>
        <label htmlFor="autocomplete-input">Service</label>
      </div>

    )
  }
}

export default AutoCompleteServiceInput


