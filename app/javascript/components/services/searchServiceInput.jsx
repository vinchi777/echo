import React, { PropTypes, Component } from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery'
import 'typeahead.js'

import Bloodhound from 'typeahead.js/dist/bloodhound.min'

class SearchServiceInput extends Component {
  constructor(props) {
    super(props)
    this.state = { query: '' }
  }
  handleChange(e) {
    this.setState({query: e.target.value})
  }
  componentDidMount(){
    var _this = this
    var suggestions = {
      query: "d"
    };

    var suggests = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('type'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      limit: 10,
      remote:  {
        url: '/services/search?query=%QUERY',
        wildcard: '%QUERY',
        transform: (response) => {
          return response.results
        }
      }
    })

    var template = _.template('<div class="type clickable"><%= type %></div>')

    $(ReactDOM.findDOMNode(this.refs.suggestion)).typeahead(
      {
        hint: true,
        highlight: true,
      },
      {
        type: 'suggests',
        displayKey: 'type',
        source: suggests,
        templates: {
          suggestion: function (data) {
            return template(data);
          }
        }

      }
    ).bind('typeahead:selected', function (obj, datum) {
      if (_this.props.redirect_on_select == 'true') {
        window.location.href = '/requests/new?service_type=' + datum.type;
      }
    });
  }
  render() {

    return (
      <div>
          <input name={this.props.name} id="search" ref="suggestion" className="form-control suggestions" type={this.props.type} placeholder={this.props.placeholder}
          value={this.state.query}
          onChange={this.handleChange.bind(this)} onBlur={this.handleChange.bind(this)}/>
          <label className="label-icon" htmlFor="search"><i className="material-icons search-icon">{ this.props.icon }</i></label>
      </div>

    )
  }
}

SearchServiceInput.defaultProps = {
  icon: 'search',
  placeholder: 'Search',
  type: 'search'
}

export default SearchServiceInput


