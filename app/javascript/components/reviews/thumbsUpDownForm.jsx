import React, { PropTypes, Component } from 'react'

export default class ThumbsUpDownForm extends Component {
  constructor( props ){
    super(props)
    this.state = {
      good: false,
      bad: false
    }
  }
  toggleThumb(review) {
    this.setState({
      good: (review == 'good'),
      bad: (review == 'bad')
    })
  }
  thumbGoodClass() {
    if (this.state.good) return 'green-text material-icons large'
    return 'grey-text material-icons large'
  }
  thumbBadClass() {
    if (this.state.bad) return 'red-text material-icons large'
    return 'grey-text material-icons large'
  }

  render() {
    return(
      <div className='row'>
        <div className="col s6 m6 center">
          <label htmlFor='review_good' className='clickable'>
            <i className={ this.thumbGoodClass() }>thumb_up</i>
            <input type='checkbox' name='review[good]'id='review_good' checked={ this.state.good } onChange={ this.toggleThumb.bind(this, 'good') } />
          </label>
        </div>

        <div className='col s6 m6 center'>
          <label htmlFor='review_bad' className='clickable'>
            <i className={ this.thumbBadClass() }>thumb_down</i>
            <input type='checkbox' name='review[bad]' id='review_bad' checked={ this.state.bad } onChange={ this.toggleThumb.bind(this, 'bad') } />
          </label>
        </div>
      </div>

    )
  }

}
