class NotificationsController < ApplicationController
  after_filter :mark_read, only: [:index]

  def index
    @recipient_activities = PublicActivity::Activity
    .order('created_at DESC')
    .where(recipient_id: current_user.id)
    .page(params[:page]).per(50)
  end

  private

  def mark_read
    @recipient_activities.update_all(recipient_read: true)
  end
end
