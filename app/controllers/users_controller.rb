class UsersController < ApplicationController

  def show
    @user = current_user
    @owner_activities = PublicActivity::Activity
    .order('created_at DESC')
    .where(owner_id: current_user.id)
    .page(params[:page]).per(50)
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'Successfully editied profile' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  private

  def user_params
    params.fetch(:user, {}).permit(:first_name, :last_name, :email, :phone, :avatar)
  end

end
