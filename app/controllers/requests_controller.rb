class RequestsController < ApplicationController
  before_action :create_user, only: [:create]
  before_action :set_request, only: [:show, :edit, :update, :destroy, :proposals]
  after_action  :save_images, only: [:update, :create]

  # GET /requests
  # GET /requests.json
  def index
    if params['commit'] == 'Search'
      @service_ids = Service.where(:category => params[:service].downcase).pluck(:id)
      @other_requests = Request.where(search_params)
    else
      @other_requests = Request.all
    end

    @other_requests = @other_requests.where(:user_id.ne => current_user.id) if current_user
  end

  def owner
    @my_requests = current_user.requests
    @recommended_services =  Service.limit(10)
  end

  # GET /requests/1
  # GET /requests/1.json
  def show
  end

  # GET /requests/new
  def new
    @request = Request.new
    @request.service = Service.where(:type => params[:service_type]).first || not_found
    @request.build_user if !current_user
  end

  # GET /requests/1/edit
  def edit
  end

  # POST /requests
  # POST /requests.json
  # TODO register user here
  def create
    @request = @user.requests.new(request_params)

    respond_to do |format|
      form_valid = @user.valid? && @request.valid?

      if form_valid && @request.save
        sign_in @user

        format.html { redirect_to @request, notice: 'Request was successfully created.' }
        format.json { render :show, status: :created, location: @request }
      else
        @request.user = @user
        format.html { render :new }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /requests/1
  # PATCH/PUT /requests/1.json
  def update
    respond_to do |format|
      if @request.update(request_params)
        format.html { redirect_to @request, notice: 'Request was successfully updated.' }
        format.json { render :show, status: :ok, location: @request }
      else
        format.html { render :edit }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    @request.destroy
    respond_to do |format|
      format.html { redirect_to requests_url, notice: 'Request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def proposals
    @proposals = @request.proposals
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_request
    @request = Request.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def request_params
    params.require(:request).permit(:service_id, :location, :title, :description, :min_price, :max_price, :start_date, :job_completion, :job_completion_unit, :payment_mode)
  end

  def create_user
    if current_user
      @user = current_user
      @user.is_customer = true
      @user.current_role = 'customer'
    else
      @user = User.new(params[:request].require(:user).permit(:first_name, :last_name, :email, :phone, :password).merge(is_customer: true, current_role: 'customer'))
    end
  end

  def save_images
    return if !@request.valid?
    (params[:request][:images] || []).each do |image|
      ri = @request.request_images.new
      ri.image = image
      ri.save
    end
  end

  def search_params
    query = {}
    query.merge!( :service_id.in => @service_ids ) if params[:service].present?
    query.merge!( :location => params[:location] ) if params[:location].present?
    query
  end
end
