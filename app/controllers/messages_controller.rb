class MessagesController < ApplicationController

  def index
    @messages = Conversation.find(params[:conversation_id]).messages
  end

  def create
    @conversation = Conversation.find(params[:message][:conversation_id])
    @message = Message.new(message_params)
    @message.user = current_user
    @message.user_role = current_user.business == @conversation.proposal.business ? 'seller' : 'customer'

    if @message.save
      ActionCable.server.broadcast "conversation_#{@message.conversation_id}_channel", JSON.parse(render(:show).first)
      head :ok
    else
      ActionCable.server.broadcast "conversation_#{@message.conversation_id}_channel", errors: @message.errors.full_messages.join(', ')
    end
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.fetch(:message, {}).permit(:body, :conversation_id)
  end

end
