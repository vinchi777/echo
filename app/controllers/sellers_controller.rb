class SellersController < ApplicationController

  def dashboard
    @requests = Request.all
    @business = current_user.business
  end

  def proposals
    @proposals = current_user.business.proposals
  end

end
