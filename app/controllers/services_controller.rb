class ServicesController < ApplicationController 

  def categories
    @services = {}
    Service.all.each do |s|
      @services[s.category.capitalize] = s.image.url
    end
    @services
  end

  def area
    @services = Service.where(area: params[:area])
  end

  def category
    @services = Service.where(area: params[:area], category: params[:category])
  end

  def search
    query = params[:query] || ""
    @services = Service.where(type: /.*#{query}.*/i)
  end

end
