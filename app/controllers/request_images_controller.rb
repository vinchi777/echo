class RequestImagesController < ApplicationController
  before_action :set_request_image, only: [:show, :edit, :update, :destroy]

  # GET /request_images
  # GET /request_images.json
  def index
    @request_images = RequestImage.all
  end

  # GET /request_images/1
  # GET /request_images/1.json
  def show
  end

  # GET /request_images/new
  def new
    @request_image = RequestImage.new
  end

  # GET /request_images/1/edit
  def edit
  end

  # POST /request_images
  # POST /request_images.json
  def create
    @request_image = RequestImage.new(request_image_params)

    respond_to do |format|
      if @request_image.save
        format.html { redirect_to @request_image, notice: 'Request image was successfully created.' }
        format.json { render :show, status: :created, location: @request_image }
      else
        format.html { render :new }
        format.json { render json: @request_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /request_images/1
  # PATCH/PUT /request_images/1.json
  def update
    respond_to do |format|
      if @request_image.update(request_image_params)
        format.html { redirect_to @request_image, notice: 'Request image was successfully updated.' }
        format.json { render :show, status: :ok, location: @request_image }
      else
        format.html { render :edit }
        format.json { render json: @request_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /request_images/1
  # DELETE /request_images/1.json
  def destroy
    @request_image.destroy
    respond_to do |format|
      format.html { redirect_to edit_request_path(@request_image.request), notice: 'Request image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request_image
      @request_image = RequestImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_image_params
      params.fetch(:request_image, {})
    end
end
