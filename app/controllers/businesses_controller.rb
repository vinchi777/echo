class BusinessesController < ApplicationController
  before_action :create_user, only: [:create]
  before_action :set_business, only: [:show, :edit, :update, :destroy]

  def show
  end

  def index
  end

  def new
    if current_user && current_user.business
      redirect_to edit_business_path(current_user.business) and return
    end

    @business = Business.new
    @business.build_user if !current_user

    if params[:request_id]
      @request = Request.find(params[:request_id])
      @service = Service.find(@request.service.id)
      @business.service_ids << @service.id
    end
  end


  def create
    @business = @user.build_business(business_params)

    if @user.valid? && @business.valid?
      if @user.new_record?
        @user.save
        sign_in @user
      end

      @business.save

      resource_path =
        if params[:request_id].present?
          new_proposal_path(request_id: params[:request_id])
        else
          @business
        end

      redirect_to resource_path, notice: 'Business was successfully created.'
    else
      @business.user = @user
      render :new
    end
  end

  def update
    respond_to do |format|
      if @business.update(business_params)
        format.html { redirect_to edit_business_path(@business) }
        format.json { render :show, status: :ok, location: @business }
      else
        format.html { render :edit }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_business
    @business = Business.find(params[:id])
  end

  def business_params
    params.require(:business).permit(:name, :image, service_ids: [])
  end

  def create_user
    if current_user
      @user = current_user
      @user.is_seller = true
      @user.current_role = 'seller'
    else
      @user = User.new(params[:business].require(:user).permit(:first_name, :last_name, :email, :phone, :password).merge(is_seller: true, current_role: 'seller'))
    end
  end
end
