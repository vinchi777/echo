class CustomersController < ApplicationController

  def dashboard
    @requests = current_user.requests
    @recommended_services =  Service.limit(10)
  end
end
