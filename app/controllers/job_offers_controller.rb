class JobOffersController < ApplicationController
  before_action :set_job_offer, only: [:show, :edit, :update, :destroy, :accept]

  # GET /job_offers
  # GET /job_offers.json
  def index
    @job_offers = JobOffer.all
  end

  # GET /job_offers/1
  # GET /job_offers/1.json
  def show
  end

  # GET /job_offers/new
  def new
    @proposal = Proposal.find(params[:proposal_id])
    @job_offer = JobOffer.new(proposal_id: @proposal.id, request_id: @proposal.request.id)
  end

  # GET /job_offers/1/edit
  def edit
  end

  # POST /job_offers
  # POST /job_offers.json
  def create
    @proposal = Proposal.find(params[:job_offer][:proposal_id])
    @job_offer = JobOffer.new(job_offer_params)
    @job_offer.proposal = @proposal
    @job_offer.request = @proposal.request

    respond_to do |format|
      if @job_offer.save
        format.html { redirect_to @job_offer, notice: 'Job offer sent to worker.' }
        format.json { render :show, status: :created, location: @job_offer }
      else
        format.html { render :new }
        format.json { render json: @job_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_offers/1
  # PATCH/PUT /job_offers/1.json
  def update
    respond_to do |format|
      if @job_offer.update(job_offer_params)
        format.html { redirect_to @job_offer, notice: 'Job offer was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_offer }
      else
        format.html { render :edit }
        format.json { render json: @job_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_offers/1
  # DELETE /job_offers/1.json
  def destroy
    @job_offer.destroy
    respond_to do |format|
      format.html { redirect_to job_offers_url, notice: 'Job offer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def accept
    respond_to do |format|
      if @job_offer.accept!
        format.html { redirect_to @job_offer, notice: 'Job offer Accepted!' }
        format.json { render :show, status: :ok, location: @job_offer }
      else
        format.html { render :show, notice: 'Failed to accept job order' }
        format.json { render json: @job_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_offer
      @job_offer = JobOffer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_offer_params
      params.fetch(:job_offer, {}).permit(:agreed_job_completion, :agreed_job_completion_unit, :agreed_price, :proposal_id, :agreed_payment_mode)
    end
end
