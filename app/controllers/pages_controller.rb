class PagesController < ApplicationController

  def home
    @home_services = Service.where(:area => 'home').group_by(&:category)
    @events_services = Service.where(:area => 'events').group_by(&:category)
    @self_services = Service.where(:area => 'self').group_by(&:category)
    if current_user
      @requests = Request.where(:user_id.ne => current_user.id).limit(6)
    else
      @requests = Request.limit(6)
    end
  end

  def how_it_works
  end
end
