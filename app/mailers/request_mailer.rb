class RequestMailer < ApplicationMailer
  default from: 'Serbreeze <donotreply@serbreeze.com>'

  def request_completion(request)
    @user = request.user
    @request = request

    mail to: request.user.email, subject: 'Your request has been completed'
  end

  def proposal_received(proposal)
    @proposal = proposal
    mail to: @proposal.request.user.email, subject: 'You received a Proposal'
  end
end
