class ProposalMailer < ApplicationMailer
  default from: 'Serbreeze <donotreply@serbreeze.com>'

  def proposal_sent(proposal)
    @proposal = proposal
    mail to: @proposal.business.user.email, subject: 'Your proposal has been sent!'
  end

end
