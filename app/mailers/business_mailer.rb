class BusinessMailer < ApplicationMailer
  default from: 'Serbreeze <donotreply@serbreeze.com>'

  def setup_notification(business)
    @business = business

    mail to: business.user.email, subject: "Welcome to Serbreeze"
  end
end
