class UserMailer < ApplicationMailer
  default from: 'Serbreeze <donotreply@serbreeze.com>'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.welcome_notification.subject
  #
  def welcome_notification(user)
    @greeting = "Hi"
    @user = user

    mail to: user.email, subject: "Welcome to Serbreeze!"
  end
end
