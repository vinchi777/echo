module ApplicationHelper

  def location_options
    path = "#{Rails.root}/lib/locations/provinces.yml"
    if File.exists?(path)
      YAML.load_file(path)
    else
      []
    end
  end

  def react_component(name, props = {}, options = {}, &block)
    html_options = options.reverse_merge(data: {
      react_class: name,
      react_props: (props.is_a?(String) ? props : props.to_json)
    })
    content_tag(:div, '', html_options, &block)
  end
end
